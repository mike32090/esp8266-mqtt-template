/**
 * Static files served by the configuration server.
 * DO NOT EDIT THIS FILE! Edit the files in the "static"
 * directory and run the "genstatic" script to
 * regenerate this file.
 */
#ifndef STATIC_H
#define STATIC_H

#include <ESP8266WebServer.h>
#include <pgmspace.h>


const char STATIC_INDEX_HTML[] PROGMEM =
R"END_OF_FILE(<!DOCTYPE html>
<html>
    <head>
        <title>Configuration</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="stylesheet" type="text/css" href="styles.css"/>
        <script src="script.js"></script>
    </head>
    <body>
        <div id="config" class="column">
            <h1>Configuration</h1>
            <form class="config-form">
                <fieldset>
                    <legend>WiFi</legend>
                    <div class="row">
                        <label for="wifi-ssid">SSID</label>
                        <input id="wifi-ssid" name="wifi-ssid"
                            class="field-wide" type="text" maxlength="32" />
                    </div>
                    <div class="row">
                        <label for="wifi-password">Password</label>
                        <input id="wifi-password" name="wifi-password"
                            class="field-wide" type="password" maxlength="63" />
                    </div>
                </fieldset>

                <fieldset>
                    <legend>MQTT</legend>
                    <div class="row">
                        <label for="mqtt-host">Host</label>
                        <input id="mqtt-host" name="mqtt-host"
                            class="field-wide" type="text" maxlength="128" />
                    </div>
                    <div class="row">
                        <label for="mqtt-port">Port</label>
                        <input id="mqtt-port" name="mqtt-port"
                            class="field-narrow" type="text" maxlength="5" />
                    </div>
                    <div class="row">
                        <label for="mqtt-username">Username</label>
                        <input id="mqtt-username" name="mqtt-username"
                            class="field-wide" type="text" maxlength="128" />
                    </div>
                    <div class="row">
                        <label for="mqtt-client-id">Client ID</label>
                        <input id="mqtt-client-id" name="mqtt-client-id"
                            class="field-wide" type="text" maxlength="23" />
                    </div>
                </fieldset>
    
                <fieldset>
                    <legend>Sensors</legend>
                    <div class="row">
                        <label for="sensor-interval">Interval</label>
                        <input id="sensor-interval" name="sensor-interval"
                            class="field-narrow" type="text" maxlength="5" />
                    </div>
                </fieldset>
    
                <div class="buttons">
                    <button class="button config-reset" type="button">Reset</button>
                    <button class="button config-save" type="button">Save</button> 
                </div>
            </form>
            <p class="config-message"></p>
        </div>
        <script>
            (function() {
                config.init('#config');
            })();
        </script>
    </body>
</html>
)END_OF_FILE";

const char STATIC_SCRIPT_JS[] PROGMEM =
R"END_OF_FILE(/*
 * Configuration page script.
 * This module interacts with the /config endpoint to display, modify, and
 * reset config data on the device.
 */
var config = (function() {
    var CONFIG_PATH = '/config';
    var REBOOT_PATH = '/reboot';

    // make an async HTTP request
    function sendRequest(method, path, postData, onDone) {
        var request = new XMLHttpRequest();

        request.onreadystatechange = function() {
            if (request.readyState == request.DONE) {
                if (request.status == 200) {
                    // call handler on success
                    onDone(request);
                } else if (request.status !== 0) {
                    // show error message from response
                    window.alert("Error: " + request.response);
                } else {
                    window.alert("Error communicating with device.");
                }
            }
        };

        request.open(method, path, true);

        if (postData) {
            request.setRequestHeader('Content-Type', postData.contentType);
            request.send(postData.body);
        } else {
            request.send();
        }
    }

    // convert a query string into an object
    function parseQueryString(string) {
        var object = {};
        var pairs = string.split('&');
        for (var i = 0, length = pairs.length; i < length; ++i) {
            var pair = pairs[i].split('=');
            if (pair.length != 2) {
                throw new Error('Malformatted query string data');
            }
            var key = decodeURIComponent(pair[0]);
            var value = decodeURIComponent(pair[1]);
            object[key] = value;
        }
        return object;
    }

    // convert object into a query string
    function formatQueryString(object) {
        var pairs = [];
        var properties = Object.keys(object);
        for (var i = 0, length = properties.length; i < length; ++i) {
            var property = properties[i];
            var key = encodeURIComponent(property);
            var value = encodeURIComponent(object[property]);
            pairs.push(key + '=' + value);
        }
        return pairs.join('&');
    }

    // extract form field values into an object
    function serializeForm(form) {
        var object = {};
        var fields = form.querySelectorAll(
            'input[type="text"], input[type="password"]');
        for (var i = 0, length = fields.length; i < length; ++i) {
            var field = fields[i];
            if (field.name) {
                object[field.name] = field.value;
            }
        }
        return object;
    }

    // populate form field values from object properties
    function populateForm(form, object) {
        var properties = Object.keys(object);
        for (var i = 0, length = properties.length; i < length; ++i) {
            var property = properties[i];
            var fields = form.querySelectorAll(
                'input[name="' + property + '"]');
            if (fields.length == 1) {
                var value = object[property];
                fields[0].value = value;
            }
        }
    }    
    
    // reboot the device and refresh the page
    function reboot(form, messageText) {
        sendRequest('POST', REBOOT_PATH, null,
            function(request) {
                // prevent further use of the UI on a disconnected device
                var formParent = form.parentNode;
                form.style.display = 'none'; // hide form
                messageText.innerText = 
                    'Device rebooting to client mode. Restart the device '
                    + 'in config mode and refresh this page to use the '
                    + 'web interface again.';
                messageText.style.display = 'block'; // show message
            });
    }

    // populate the form from the device config
    function loadConfig(form) {
        sendRequest('GET', CONFIG_PATH, null,
            function(request) {
                try {
                    config = parseQueryString(request.response);
                    populateForm(form, config);
                } catch(exception) {
                    window.alert(exception.message); // show error
                }
            });
    }

    // post the form config to the device
    function saveConfig(form, messageText) {
        // confirm with dialog
        if (!window.confirm("Save settings?")) {
            return;
        }

        // extract form fields and format as query string
        var config = serializeForm(form);
        var formattedConfig = formatQueryString(config);
        var postData = {
            contentType: "application/x-www-form-urlencoded",
            body: formattedConfig
        };

        // POST query string to config endpoint
        sendRequest('POST', CONFIG_PATH, postData,
            function(request) {
                // prompt to reboot on successful save
                if (window.confirm("Settings saved. Reboot into client mode?")) {
                    reboot(form, messageText);
                }
            });
    }

    // reset the config to the default values
    function resetConfig(form) {
        // confirm with dialog
        if (!window.confirm("Reset settings to defaults?")) {
            return;
        }

        sendRequest('DELETE', CONFIG_PATH, null,
            function(request) {
                // populate form with new config
                loadConfig(form);
            });
    }

    return {
        init: function(selector) {
            var config = document.querySelector(selector);
            var form = config.querySelector(".config-form");
            var resetButton = config.querySelector(".config-reset");
            var saveButton = config.querySelector(".config-save");
            var messageText = config.querySelector(".config-message");

            // register reset button
            resetButton.addEventListener('click', function(event) {
                resetConfig(form);               
            });

            // register apply button 
            saveButton.addEventListener('click', function(event) {
                saveConfig(form, messageText);
            });

            // populate form
            loadConfig(form);
        }
    }
})();
)END_OF_FILE";

const char STATIC_STYLES_CSS[] PROGMEM =
R"END_OF_FILE(body {
    background: #111111;
    color: #eeeeee;
    font-size: 12pt;
    font-family: sans-serif;
    line-height: 1.2;
    margin: 1.5em 0.5em;
    padding: 0; 
}

h1 {
    font-size: 2em;
    text-align: center;
    margin: 0;
}
p {
    margin: 1em 0;
}

fieldset {
    margin: 1em 0 1em;
    padding: 1em;
    outline: 0;
    border: 1px solid #666666;
    border-radius: 4px;
}
legend {
    font-weight: bold;
}
label {
    display: inline-block;
    width: 6em;
    margin-right: 0.5em;
    text-align: right;
}
input, button {
    background: #222222;
    color: #ffffff;
    margin: 0;
    padding: 0.25em 0.5em;
    outline: 0;
    border: 1px solid #666666;
    border-radius: 2px;
    font-size: 1em;
}
button {
    background: #333333;
    width: 80px;
    margin: 0 0.25em;
}


.column {
    margin: 0 auto;
    max-width: 400px;
}

.row {
    display: flex;
    align-items: center;
    justify-content: flex-start;
    margin: 0.5em 0;
    min-height: 2em;
}



.field-wide {
    flex: auto;
    width: 0;
    min-width: 0;
    overflow: hidden;
}
.field-narrow {
    flex: auto;
    max-width: 80px;
}

.buttons {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 1.5em;
}

.config-message {
    display: none;
    text-align: center;
}
)END_OF_FILE";

void staticRegisterAll(ESP8266WebServer* webServer) {
    webServer->on("/", [webServer]() { webServer->send_P(200, "text/html; charset=UTF-8", STATIC_INDEX_HTML); });
    webServer->on("/script.js", [webServer]() { webServer->send_P(200, "application/javascript", STATIC_SCRIPT_JS); });
    webServer->on("/styles.css", [webServer]() { webServer->send_P(200, "text/css", STATIC_STYLES_CSS); });
}

#endif
