/**
 * Config mode web server.
 */
#ifndef SERVER_H
#define SERVER_H

#include "eeprom.h"

#include <ESP8266WebServer.h>
#include <WString.h>


/**
 * Config mode web server.
 * This server hosts static web content as well as a config API.
 */
class ConfigServer {
    public:
        ConfigServer(EepromStore& eepromStore);
        void setup();
        void loop();

    private:
        EepromStore& eepromStore;
        ESP8266WebServer webServer;
        void handleConfig();
        void handleReboot();
        void urlAppend(const char* value, String& buffer);
};

#endif
