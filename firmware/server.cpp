#include "server.h"
#include "static.h"
#include "config.h"

#include <Esp.h>
#include <ESP8266WiFi.h>
#include <WString.h>

#include <pgmspace.h>

#include <cstdio>
#include <cstring>


// WiFi access point settings
const char AP_SSID_PREFIX[] = "Sensor";  // suffixed by WiFi MAC
const char AP_PASSWORD[] = "password";  // must be 8+ chars

// network settings
const IPAddress AP_IP(192, 168, 1, 1);  // address of web interface
const IPAddress AP_GATEWAY(192, 168, 1, 1);
const IPAddress AP_SUBNET(255, 255, 255, 0);

// web server settings
const std::uint16_t WEB_PORT = 80;

// used for URL encoding (% must be first character in this string)
const char URL_RESERVED_CHARS[] = "%!#$&'()*+,/:;=?@[]";


ConfigServer::ConfigServer(EepromStore& eepromStore)
        : eepromStore(eepromStore), webServer(AP_IP, WEB_PORT) {}

void ConfigServer::setup() {
    // generate SSID of prefix + MAC
    std::uint8_t mac[6];
    char ssid[sizeof(AP_SSID_PREFIX) + sizeof(mac) * 2]; // each MAC byte is 2 hex chars
    WiFi.macAddress(mac);
    std::sprintf(ssid, "%s:%02X%02X%02X%02X%02X%02X",
        AP_SSID_PREFIX, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

    // bring WiFi up in AP mode
    WiFi.mode(WIFI_AP);
    WiFi.softAP(ssid, AP_PASSWORD);
    WiFi.softAPConfig(AP_IP, AP_GATEWAY, AP_SUBNET);

    // register paths and start web server
    this->webServer.on("/config", [this]() { this->handleConfig(); });
    this->webServer.on("/reboot", [this]() { this->handleReboot(); });
    staticRegisterAll(&this->webServer); // serve all pages in static.h
    this->webServer.begin();
}

void ConfigServer::loop() {
    this->webServer.handleClient();
}

// handle a GET, POST, or DELETE to /config
void ConfigServer::handleConfig() {
    if (this->webServer.method() == HTTP_GET) {
        // load config from EEPROM
        config_values storedValues;
        this->eepromStore.read(storedValues);
        StringConfig stringConfig(storedValues);

        // build response body
        String body;
        for (std::size_t i = 0; i < StringConfig::USER_FIELD_COUNT; ++i) {
            const char* fieldName = StringConfig::USER_FIELDS[i];
            char formattedValue[CONFIG_BUFFER_SIZE];
            stringConfig.format(fieldName, formattedValue);

            this->urlAppend(fieldName, body);
            body += "=";
            this->urlAppend(formattedValue, body);
            if (i < StringConfig::USER_FIELD_COUNT - 1) {
                body += "&";
            }
        }
        this->webServer.send(200, "application/x-www-form-urlencoded", body);

    } else if (this->webServer.method() == HTTP_POST) {
        config_values postedValues = {};
        StringConfig stringConfig(postedValues);

        // parse all uploaded fields
        for (std::size_t i = 0; i < this->webServer.args(); i++) {
            const String key = this->webServer.argName(i);
            const String value = this->webServer.arg(i);

            // skip the ESP8266WebServer generated "plain" field
            if (key == "plain") {
                continue;
            }

            // report parsing error to the web interface
            PGM_P error = stringConfig.parse(key.c_str(), value.c_str());
            if (error != NULL) {
                this->webServer.send_P(400, "text/plain", error);
                return;
            }
        }

        // make sure all required fields are set
        PGM_P error = stringConfig.isValid();
        if (error != NULL) {
            this->webServer.send_P(400, "text/plain", error);
            return;
        }

        // write parsed fields to EEPROM
        postedValues.mode = MODE_CLIENT;  // set to boot into client mode
        this->eepromStore.write(postedValues);

        this->webServer.send(200);

    } else if (this->webServer.method() == HTTP_DELETE) {
        // reset to default configuration
        this->eepromStore.writeDefaults();
        this->webServer.send(200);

    } else {
        this->webServer.send(405, "text/plain", "HTTP method not allowed");
    }
}

// handle a POST to /reboot
void ConfigServer::handleReboot() {
    if (this->webServer.method() == HTTP_POST) {
        this->webServer.send(200);
        delay(1000); // wait for response to be sent
        ESP.restart();
    } else {
        this->webServer.send(405, "text/plain", "HTTP method not allowed");
    }
}

// URL encode value and append to buffer
void ConfigServer::urlAppend(const char* value, String& buffer) {
    String stringValue = String(value);
    for (std::size_t i = 0; i < std::strlen(URL_RESERVED_CHARS); ++i) {
        char reservedChar = URL_RESERVED_CHARS[i];
        String reservedString(reservedChar);
        String encodedChar((unsigned int)reservedChar, HEX);
        stringValue.replace(reservedString, "%" + encodedChar);
    }
    buffer += stringValue;
}
