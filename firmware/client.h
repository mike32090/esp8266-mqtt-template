/**
 * .
 */
#ifndef CLIENT_H
#define CLIENT_H

#include "eeprom.h"

#include <ESP8266WiFi.h>

#include <PubSubClient.h>


/**
 * .
 */
class MqttClient {
    public:
        MqttClient(EepromStore& eepromStore);
        void setup();
        void loop();

    private:
        EepromStore& eepromStore;
        config_values configValues;
        uint32_t lastPublishTime;
        WiFiClient wifiClient;
        PubSubClient pubSubClient;
        void publish();
};

#endif
