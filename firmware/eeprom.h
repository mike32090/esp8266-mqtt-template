/**
 * Configuration storage in EEPROM.
 */
// Arduino std lib uses "EEPROM_H"
#ifndef EEPROM_H_
#define EEPROM_H_

#include "config.h"

#include <cstdint>
#include <cstddef>


// length of MD5 hash array in bytes
const std::size_t HASH_SIZE = 16 * sizeof(uint8_t);


/**
 * Loads and saves values in "EEPROM" (SPI Flash on the ESP8266).
 * Other modules should use the "EepromStore" instance provided by this header.
 */
class EepromStore {
    public:
        /**
         * Initialize EEPROM library and write default configuration if existing
         * one is invalid. An invalid configuration will be present after the first
         * firmware flash and when the config_values struct fields change between
         * flashes.
         */
        void begin();

        /**
         * Read only the mode field from the stored config.
         */
        std::uint8_t readMode();

        /**
         * Read the config values from EEPROM.
         */
        void read(config_values& values);

        /**
         * Write the config values to EEPROM.
         */
        void write(const config_values& values);

        /**
         * Write the default config values to EEPROM.
         */
        void writeDefaults();

    private:
        bool isValid();
        void calcHash(const config_values& values, std::uint8_t hash[HASH_SIZE]);
};

#endif
