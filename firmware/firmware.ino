/**
 * Main file for ESP8266 MQTT firmware.
 */
#include "server.h"
#include "client.h"
#include "config.h"
#include "reset.h"
#include "eeprom.h"

#include <Esp.h>
#include <ESP8266WiFi.h>

#include <cstdint>


// LED used to indicate resets
const std::uint8_t BLINK_PIN = 16;  // NodeMCU LED
const std::uint16_t BLINK_PERIOD_MILLIS = 200;


EepromStore eepromStore;
ResetDetector resetDetector;
ConfigServer configServer(eepromStore);
MqttClient mqttClient(eepromStore);

std::uint8_t mode; // operation mode


// blink the LED
void blink(std::uint8_t count) {
    while (count--) {
        digitalWrite(BLINK_PIN, LOW);  // on
        delay(BLINK_PERIOD_MILLIS);
        digitalWrite(BLINK_PIN, HIGH);  // off
        delay(BLINK_PERIOD_MILLIS);
    }
}


void setup() {
    // enable on-board LED
    digitalWrite(BLINK_PIN, HIGH);  // off
    pinMode(BLINK_PIN, OUTPUT);

    // set global WiFi settings
    WiFi.persistent(false);  // already saved in flash, see config.h
    WiFi.mode(WIFI_OFF); // disable until a mode is entered

    // read mode from stored config
    eepromStore.begin(); // writes defaults on first run
    mode = eepromStore.readMode();

    // handle resets
    std::uint8_t totalResets = resetDetector.count();
    if (totalResets == 3) {
        mode = MODE_CONFIG;
        blink(3);
    } else if (totalResets == 5) {
        eepromStore.writeDefaults();
        mode = eepromStore.readMode();
        blink(5);
    }

    // start appropriate mode
    if (mode == MODE_CONFIG) {
        configServer.setup();

    } else if (mode == MODE_CLIENT) {
        mqttClient.setup();
    }
}

void loop() {
    if (mode == MODE_CONFIG) {
        configServer.loop();
    } else if (mode == MODE_CLIENT) {
        mqttClient.loop();
    }
}
