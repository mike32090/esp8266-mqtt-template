#include "eeprom.h"
#include "config.h"

#include <cstddef>
#include <cstring>

#include <EEPROM.h>
#include <md5.h>


/**
 * EEPROM memory offset for storage of config struct
 */
const std::size_t HASH_OFFSET = 0;
const std::size_t CONFIG_OFFSET = HASH_OFFSET + HASH_SIZE;


void EepromStore::begin() {
    EEPROM.begin(HASH_SIZE + sizeof(config_values));
    if (!this->isValid()) {
        this->writeDefaults();
    }
}

std::uint8_t EepromStore::readMode() {
    size_t modeOffset = CONFIG_OFFSET + offsetof(config_values, mode);
    std::uint8_t mode;
    EEPROM.get(modeOffset, mode);
    return mode;
}

void EepromStore::read(config_values& values) {
    EEPROM.get(CONFIG_OFFSET, values);
}

void EepromStore::write(const config_values& values) {
    std::uint8_t hash[HASH_SIZE];
    this->calcHash(values, hash);
    EEPROM.put(HASH_OFFSET, hash);
    EEPROM.put(CONFIG_OFFSET, values);
    EEPROM.commit();
}

void EepromStore::writeDefaults() {
    config_values defaultValues = CONFIG_DEFAULT_VALUES;
    this->write(defaultValues);
}

// compare stored checksum with calculated checksum to determine EEPROM data integrity
bool EepromStore::isValid() {
    // load hash and config from EEPROM
    std::uint8_t storedHash[HASH_SIZE];
    config_values storedValues;
    EEPROM.get(HASH_OFFSET, storedHash);
    EEPROM.get(CONFIG_OFFSET, storedValues);

    // calculate hash of stored config
    std::uint8_t calculatedHash[HASH_SIZE];
    this->calcHash(storedValues, calculatedHash);

    // compare hashes
    int compare = std::memcmp(storedHash, calculatedHash, sizeof(storedHash));
    return compare == 0;
}

// calculate hash of config values
void EepromStore::calcHash(const config_values& values, std::uint8_t hash[HASH_SIZE]) {
    md5_context_t hashContext;
    MD5Init(&hashContext);
    MD5Update(&hashContext, (std::uint8_t*)&values, sizeof(config_values));
    MD5Final(hash, &hashContext);
}
