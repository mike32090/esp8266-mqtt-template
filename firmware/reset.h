/**
 * Multiple reset detector.
 * This detector works by using RTC memory to store a reset counter that is
 * incremented on startup and reset after a blocking delay. Consecutive resets
 * will interrupt this delay and cause the counter to be incremented. The last
 * consecutive reset will allow the timer to elapse and cause the total count
 * to be reset to 0. The total count of consecutive resets will then be reported
 * to the caller.
 */
#ifndef RESET_H
#define RESET_H

#include <cstdint>
#include <cstddef>


/**
 * Consecutive reset detector.
 */
class ResetDetector {
    public:
        ResetDetector();

        /**
         * Increment the reset count and wait for the detection period.
         * If the detection period elapses the counter will be reset to 0 and
         * total count of resets will be returned.
         */
        std::uint8_t count();
};

#endif
