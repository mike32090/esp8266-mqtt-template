/**
 * Configuration struct and parsing/storage objects.
 */
#ifndef CONFIG_H
#define CONFIG_H

#include <cstdint>
#include <cstddef>

#include <pgmspace.h>


/**
 * Default config_value values.
 */
#define CONFIG_DEFAULT_VALUES {         \
    MODE_CONFIG,  /* mode */            \
    "",           /* wifiSsid */        \
    "",           /* wifiPassword */    \
    "",           /* mqttHost */        \
    1883,         /* mqttPort */        \
    "",           /* mqttUsername */    \
    "",           /* mqttClientId */    \
    10,           /* sensorInterval */  \
}


/**
 * Structure containing all config values.
 * The +1 on array sizes is just to make it easier to visually determine
 * the field content width since a nul character will be added.
 */
typedef struct config_values {
    std::uint8_t mode;

    // WiFi
    char wifiSsid[32 + 1];
    char wifiPassword[63 + 1];

    // MQTT
    char mqttHost[128 + 1];
    std::uint16_t mqttPort;
    char mqttUsername[128 + 1];
    char mqttClientId[23 + 1];

    // sensors
    std::uint16_t sensorInterval;
} config_values;


/**
 * Config mode -- start a web server for configuring device.
 */
const std::uint8_t MODE_CONFIG = 0;

/**
 * Client mode -- connect to an MQTT broker and publish updates.
 */
const std::uint8_t MODE_CLIENT = 1;

/**
 * Size of the buffer needed to retrieve values from StringConfig.
 * Set this to the length of the longest config_values field + 1
 */
const std::size_t CONFIG_BUFFER_SIZE = (128 + 1) * sizeof(char);


/**
 * Parses and formats config fields from strings.
 */
class StringConfig {
    public:
        /**
         * List of user-configurable field names.
         * This should be set to the names of the fields in the config_values
         * struct, but converted to kebab-case: wifiSsid -> wifi-ssid .
         */
        static const char* const USER_FIELDS[];
        static const std::size_t USER_FIELD_COUNT;

        /**
         * Create a formatter/parser for the specified config_values struct.
         */
        StringConfig(config_values& values);

        /**
         * Format the specified field into an output buffer.
         * Buffer must be at least CONFIG_BUFFER_SIZE bytes long.
         */
        void format(const char* fieldName, char* buffer);

        /**
         * Parse the specified field from a string value.
         * A pointer to an error message *in flash* (PGM_P) will be returned on failure.
         */
        PGM_P parse(const char* fieldName, const char* stringValue);

        /**
         * Check if all required fields have been set.
         * A pointer to an error message *in flash* (PGM_P) will be returned on failure.
         */
        PGM_P isValid();
    private:
        config_values& values;
        bool isPrintableString(const char* value);
};

#endif
