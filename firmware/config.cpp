#include "config.h"

#include <cctype>
#include <cstdio>
#include <cstring>


// names of user fields
const char WIFI_SSID[] = "wifi-ssid";
const char WIFI_PASSWORD[] = "wifi-password";
const char MQTT_HOST[] = "mqtt-host";
const char MQTT_PORT[] = "mqtt-port";
const char MQTT_USERNAME[] = "mqtt-username";
const char MQTT_CLIENT_ID[] = "mqtt-client-id";
const char SENSOR_INTERVAL[] = "sensor-interval";

// list of all names defined above
const char* const StringConfig::USER_FIELDS[] = {
    WIFI_SSID,
    WIFI_PASSWORD,
    MQTT_HOST,
    MQTT_PORT,
    MQTT_USERNAME,
    MQTT_CLIENT_ID,
    SENSOR_INTERVAL
};

const std::size_t StringConfig::USER_FIELD_COUNT =
        sizeof(StringConfig::USER_FIELDS) / sizeof(StringConfig::USER_FIELDS[0]);

StringConfig::StringConfig(config_values& values)
        : values(values) {}

void StringConfig::format(const char* fieldName, char* buffer) {
    if (std::strcmp(fieldName, WIFI_SSID) == 0) {
        std::strncpy(buffer, this->values.wifiSsid, CONFIG_BUFFER_SIZE);

    } else if (std::strcmp(fieldName, WIFI_PASSWORD) == 0) {
        std::strncpy(buffer, this->values.wifiPassword, CONFIG_BUFFER_SIZE);

    } else if (std::strcmp(fieldName, MQTT_HOST) == 0) {
        std::strncpy(buffer, this->values.mqttHost, CONFIG_BUFFER_SIZE);

    } else if (std::strcmp(fieldName, MQTT_PORT) == 0) {
        std::sprintf(buffer, "%u", this->values.mqttPort);

    } else if (std::strcmp(fieldName, MQTT_USERNAME) == 0) {
        std::strncpy(buffer, this->values.mqttUsername, CONFIG_BUFFER_SIZE);

    } else if (std::strcmp(fieldName, MQTT_CLIENT_ID) == 0) {
        std::strncpy(buffer, this->values.mqttClientId, CONFIG_BUFFER_SIZE);

    } else if (std::strcmp(fieldName, SENSOR_INTERVAL) == 0) {
        std::sprintf(buffer, "%u", this->values.sensorInterval);

    } else {
        // default to empty string if field isn't found
        buffer[0] = '\0';
    }
}

PGM_P StringConfig::parse(const char* fieldName, const char* stringValue) {
    std::size_t valueLength = std::strlen(stringValue);

    if (std::strcmp(fieldName, WIFI_SSID) == 0) {
        if (valueLength > 32) {
            return PSTR("WiFi SSID must not be longer than 32 characters.");
        }
        if (!this->isPrintableString(stringValue)) {
            return PSTR("WiFi SSID must only contain printable ASCII characters.");
        }
        std::strncpy(this->values.wifiSsid, stringValue, 32);

    } else if (std::strcmp(fieldName, WIFI_PASSWORD) == 0) {
        if (valueLength > 63) {
            return PSTR("WiFi password must not be longer than 63 characters.");
        }
        if (!this->isPrintableString(stringValue)) {
            return PSTR("WiFi password must only contain printable ASCII characters.");
        }
        std::strncpy(this->values.wifiPassword, stringValue, 63);

    } else if (std::strcmp(fieldName, MQTT_HOST) == 0) {
        if (valueLength > 128) {
            return PSTR("MQTT host must not be longer than 128 characters.");
        }
        if (!this->isPrintableString(stringValue)) {
            return PSTR("MQTT host must only contain printable ASCII characters.");
        }
        std::strncpy(this->values.mqttHost, stringValue, 128);

    } else if (std::strcmp(fieldName, MQTT_PORT) == 0) {
        unsigned int result;
        int count = std::sscanf(stringValue, "%u", &result);
        if (count != 1 || result < 1 || result > 65535) {
            return PSTR("MQTT port must be a number in the range 1 to 65535.");
        }
        this->values.mqttPort = result;

    } else if (std::strcmp(fieldName, MQTT_USERNAME) == 0) {
        if (valueLength > 128) {
            return PSTR("MQTT username must not be longer than 128 characters.");
        }
        if (!this->isPrintableString(stringValue)) {
            return PSTR("MQTT username must only contain printable ASCII characters.");
        }
        std::strncpy(this->values.mqttUsername, stringValue, 128);

    } else if (std::strcmp(fieldName, MQTT_CLIENT_ID) == 0) {
        if (valueLength > 128) {
            return PSTR("MQTT client ID must not be longer than 128 characters.");
        }
        if (!this->isPrintableString(stringValue)) {
            return PSTR("MQTT client ID must only contain printable ASCII characters.");
        }
        std::strncpy(this->values.mqttClientId, stringValue, 128);

    } else if (std::strcmp(fieldName, SENSOR_INTERVAL) == 0) {
        unsigned int result;
        int count = std::sscanf(stringValue, "%u", &result);
        if (count != 1 || result < 1 || result > 65535) {
            return PSTR("Sensor interval must be a number in the range 1 to 65535.");
        }
        this->values.sensorInterval = result;

    } else {
        return PSTR("Unknown config field.");
    }

    return NULL;  // parse success
}

PGM_P StringConfig::isValid() {
    if (std::strlen(this->values.wifiSsid) == 0) {
        return PSTR("WiFi SSID is required.");

    } else if (std::strlen(this->values.wifiPassword) == 0) {
        return PSTR("WiFi password is required.");

    } else if (std::strlen(this->values.mqttHost) == 0) {
        return PSTR("MQTT host is required.");

    } else if (values.mqttPort == 0) {
        return PSTR("MQTT port is required.");
    } else if (std::strlen(this->values.mqttClientId) == 0) {
        return PSTR("MQTT client ID is required.");

    } else if (values.sensorInterval == 0) {
        return PSTR("Sensor interval is required.");
    }

    return NULL;  // all fields valid
}

// indicate if value is a printable ASCII string
bool StringConfig::isPrintableString(const char* value) {
    for (std::size_t i = 0; i < std::strlen(value); ++i) {
        if (!std::isprint(value[i])) {
            return false;
        }
    }
    return true;
}
