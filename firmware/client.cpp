#include "client.h"

#include <Arduino.h>


// period to wait for connections to complete
std::uint16_t WAIT_DELAY_MILLIS = 500;


MqttClient::MqttClient(EepromStore& eepromStore)
        : eepromStore(eepromStore), wifiClient(wifiClient),
        pubSubClient(wifiClient) {}

void MqttClient::setup() {
    this->eepromStore.read(this->configValues);  // load config from EEPROM
    this->lastPublishTime = 0;

    // bring WiFi up in STA mode
    WiFi.mode(WIFI_STA);
    WiFi.begin(this->configValues.wifiSsid, this->configValues.wifiPassword);

    // set up MQTT client
    this->pubSubClient.setServer(this->configValues.mqttHost, this->configValues.mqttPort);
}

void MqttClient::loop() {
    // wait for WiFi connection
    while (WiFi.status() != WL_CONNECTED) {
        delay(WAIT_DELAY_MILLIS);
    }

    // (re)connect to MQTT broker
    while (!this->pubSubClient.connected()) {
        bool connected = this->pubSubClient.connect(
            this->configValues.mqttClientId, this->configValues.mqttUsername, NULL);
        if (!connected) {
            delay(WAIT_DELAY_MILLIS);
        }
    }

    // publish an update if the sensorInterval has elapsed
    uint32_t sensorIntervalMillis = this->configValues.sensorInterval * 1000;
    uint32_t elapsedMillis = millis() - this->lastPublishTime;
    if (this->lastPublishTime == 0 || elapsedMillis >= sensorIntervalMillis) {
        this->lastPublishTime = millis();
        this->publish();
    }

    this->pubSubClient.loop();
}

// publish sensor data to MQTT broker
void MqttClient::publish() {
    // TODO replace this with real sensor code
    char uptimeString[11]; // max = 2,147,483,647
    sprintf(uptimeString, "%u", millis());
    this->pubSubClient.publish("uptime", uptimeString);
}
