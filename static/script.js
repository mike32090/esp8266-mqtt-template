/*
 * Configuration page script.
 * This module interacts with the /config endpoint to display, modify, and
 * reset config data on the device.
 */
var config = (function() {
    var CONFIG_PATH = '/config';
    var REBOOT_PATH = '/reboot';

    // make an async HTTP request
    function sendRequest(method, path, postData, onDone) {
        var request = new XMLHttpRequest();

        request.onreadystatechange = function() {
            if (request.readyState == request.DONE) {
                if (request.status == 200) {
                    // call handler on success
                    onDone(request);
                } else if (request.status !== 0) {
                    // show error message from response
                    window.alert("Error: " + request.response);
                } else {
                    window.alert("Error communicating with device.");
                }
            }
        };

        request.open(method, path, true);

        if (postData) {
            request.setRequestHeader('Content-Type', postData.contentType);
            request.send(postData.body);
        } else {
            request.send();
        }
    }

    // convert a query string into an object
    function parseQueryString(string) {
        var object = {};
        var pairs = string.split('&');
        for (var i = 0, length = pairs.length; i < length; ++i) {
            var pair = pairs[i].split('=');
            if (pair.length != 2) {
                throw new Error('Malformatted query string data');
            }
            var key = decodeURIComponent(pair[0]);
            var value = decodeURIComponent(pair[1]);
            object[key] = value;
        }
        return object;
    }

    // convert object into a query string
    function formatQueryString(object) {
        var pairs = [];
        var properties = Object.keys(object);
        for (var i = 0, length = properties.length; i < length; ++i) {
            var property = properties[i];
            var key = encodeURIComponent(property);
            var value = encodeURIComponent(object[property]);
            pairs.push(key + '=' + value);
        }
        return pairs.join('&');
    }

    // extract form field values into an object
    function serializeForm(form) {
        var object = {};
        var fields = form.querySelectorAll(
            'input[type="text"], input[type="password"]');
        for (var i = 0, length = fields.length; i < length; ++i) {
            var field = fields[i];
            if (field.name) {
                object[field.name] = field.value;
            }
        }
        return object;
    }

    // populate form field values from object properties
    function populateForm(form, object) {
        var properties = Object.keys(object);
        for (var i = 0, length = properties.length; i < length; ++i) {
            var property = properties[i];
            var fields = form.querySelectorAll(
                'input[name="' + property + '"]');
            if (fields.length == 1) {
                var value = object[property];
                fields[0].value = value;
            }
        }
    }    
    
    // reboot the device and refresh the page
    function reboot(form, messageText) {
        sendRequest('POST', REBOOT_PATH, null,
            function(request) {
                // prevent further use of the UI on a disconnected device
                var formParent = form.parentNode;
                form.style.display = 'none'; // hide form
                messageText.innerText = 
                    'Device rebooting to client mode. Restart the device '
                    + 'in config mode and refresh this page to use the '
                    + 'web interface again.';
                messageText.style.display = 'block'; // show message
            });
    }

    // populate the form from the device config
    function loadConfig(form) {
        sendRequest('GET', CONFIG_PATH, null,
            function(request) {
                try {
                    config = parseQueryString(request.response);
                    populateForm(form, config);
                } catch(exception) {
                    window.alert(exception.message); // show error
                }
            });
    }

    // post the form config to the device
    function saveConfig(form, messageText) {
        // confirm with dialog
        if (!window.confirm("Save settings?")) {
            return;
        }

        // extract form fields and format as query string
        var config = serializeForm(form);
        var formattedConfig = formatQueryString(config);
        var postData = {
            contentType: "application/x-www-form-urlencoded",
            body: formattedConfig
        };

        // POST query string to config endpoint
        sendRequest('POST', CONFIG_PATH, postData,
            function(request) {
                // prompt to reboot on successful save
                if (window.confirm("Settings saved. Reboot into client mode?")) {
                    reboot(form, messageText);
                }
            });
    }

    // reset the config to the default values
    function resetConfig(form) {
        // confirm with dialog
        if (!window.confirm("Reset settings to defaults?")) {
            return;
        }

        sendRequest('DELETE', CONFIG_PATH, null,
            function(request) {
                // populate form with new config
                loadConfig(form);
            });
    }

    return {
        init: function(selector) {
            var config = document.querySelector(selector);
            var form = config.querySelector(".config-form");
            var resetButton = config.querySelector(".config-reset");
            var saveButton = config.querySelector(".config-save");
            var messageText = config.querySelector(".config-message");

            // register reset button
            resetButton.addEventListener('click', function(event) {
                resetConfig(form);               
            });

            // register apply button 
            saveButton.addEventListener('click', function(event) {
                saveConfig(form, messageText);
            });

            // populate form
            loadConfig(form);
        }
    }
})();
